const clock= new THREE.Clock();
let time= 0;
let scene, camera, renderer;
let target={x:2,y:2,z:2};
let camera_offset={x:10,y:10,z:10};
let camera_speed=.1;

const getRandomParticlePos = (particleCount) => {
	const arr = new Float32Array(particleCount * 3);
	for (let i = 0; i < particleCount; i++) {
		arr[i] = (Math.random() - 0.5) * 100;
	}
	return arr;
};


function init() {
	scene = new THREE.Scene();
	scene.background = new THREE.Color(0x070a17);

	camera = new THREE.PerspectiveCamera(120, window.innerWidth/window.innerHeight, 1, 5000);

	hlight = new THREE.AmbientLight (0x070a17, 2);
	scene.add(hlight);

	directionalLight = new THREE.DirectionalLight(0x070a17, .2);
	directionalLight.position.set(0,100,0);
	directionalLight.castShadow = true;
	scene.add(directionalLight);

	light = new THREE.PointLight(0x070a17,.4);
	light.position.set(0,300,500);
	scene.add(light);

	renderer = new THREE.WebGLRenderer({antialias:true});
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);


	const geometry = new THREE.BufferGeometry();
	const noOfPoints = 500;
	geometry.setAttribute(
		"position",
		new THREE.BufferAttribute(getRandomParticlePos(noOfPoints), 3)
	);

	const material= new THREE.PointsMaterial({
		size: 0.05
	});

	// create a Mesh
	const cube = new THREE.Points(geometry, material);

	scene.add(cube);

	controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls.addEventListener('change', renderer);

	let loader = new THREE.FBXLoader();
	loader.load('lowquad2.fbx', function(object){
		scene.add(object);
		animate();
	});
}
function animate(){
	requestAnimationFrame(animate);

	clock.getDelta();
	time=clock.elapsedTime.toFixed(2);

	camera.position.x=target.x+camera_offset.x*(Math.sin(time*camera_speed));
	camera.position.z=target.z+camera_offset.z*(Math.cos(time*camera_speed));
	camera.position.y=target.y+camera_offset.y;
	camera.lookAt(target.x,target.y,target.z);

	renderer.render(scene,camera);

}
init();

document.addEventListener('DOMContentLoaded', () => {
	const juneThird2024 = new Date('2024-06-03T00:00:00Z').getTime() / 1000;
	new FlipDown(juneThird2024)
		.start()

		// Do something when the countdown ends
		.ifEnded(() => {
			console.log('The countdown has ended!');
		});

});
