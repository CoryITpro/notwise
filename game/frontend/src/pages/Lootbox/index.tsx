import { Link } from "react-router-dom";
import styled from "styled-components";
import Vector from "../../assets/Vector.svg";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 32px 16px;
  width: 100%;
  height: 100%;
  gap: 32px;
`;

export const Lootbox = () => {

  return (
    <Wrapper className="bg-gradient-to-t from-[#5984AB] to-[#242424]">
      <div className="w-full">
        <div className="flex gap-2 mb-3">
          <button className="w-4 h-full"><img src={Vector} alt="Vector" /></button>
        </div>
        <div className="flex flex-col justify-center items-center gap-6 px-2">
          <div className="w-full px-2">
            <div className="h-[200px] bg-[#D9D9D9] rounded-[36px]"></div>
          </div>
          <div className="w-full text-center">
            <h4 className="text-2xl font-bold text-white mb-1">Item</h4>
            <p className="text-sm font-normal text-white">Item description</p>
          </div>
        </div>
      </div>
      <div className="w-full my-4">
        <button className="w-full text-xs font-normal text-white bg-[#0094FF] rounded-md px-4 py-3">Open</button>
      </div>
    </Wrapper>
  );
};

export default Lootbox;
