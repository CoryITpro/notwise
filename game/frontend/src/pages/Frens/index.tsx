import { Link } from "react-router-dom";
import styled from "styled-components";
import Vector from "../../assets/Vector.svg";
import Branch from "../../assets/icon-branch.svg";
import { FRENS_ARRAY } from "../../constants";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 32px 16px;
  width: 100%;
  height: 100%;
`;

export const Frens = () => {
  return (
    <Wrapper className="bg-gradient-to-t from-[#5984AB] to-[#242424]">
      <div className="w-full">
        <div className="flex items-center gap-2 mb-1">
          <button className="w-4 h-full"><img src={Vector} alt="Vector" /></button>
          <h6 className="text-2xl font-bold text-white">Frens</h6>
        </div>
        <div className="flex flex-col justify-center items-center mt-6 mb-8 gap-1">
          <div className="gap-2">
            <p className="text-xs font-normal text-white text-center opacity-40">total earned</p>
            <div className="flex justify-center items-center gap-2 text-2xl font-bold text-white">
              <div className="flex justify-center items-center w-4 h-4 rounded-full bg-[#FFB800]">
                <div className="coin__front rounded-full w-[10px] h-[10px] bg-[#FF9900]"></div>
              </div>
              12333
            </div>
          </div>
        </div>

        <div className="max-h-64 overflow-y-auto scroll-mr-6">
          {
            FRENS_ARRAY.map((item, index) => (
              <div className="grid grid-cols-12 items-center gap-2 mb-2" key={index}>
                <div className="col-span-1 text-[10px] font-bold text-white">
                  <Link to={`/squad/${index+1}`} >#{index+1}</Link>
                </div>
                <div className="col-span-5">
                  <div className="flex items-center gap-2">
                    <div className="flex justify-center items-center w-3 h-3 rounded-full bg-[#D9D9D9]">
                    </div>
                    <h6 className="text-xs font-normal text-white opacity-60">{item.username}</h6>
                  </div>
                </div>
                <div className="col-span-3">
                  <div className="flex items-center gap-1 text-xs font-bold text-white">
                    <div className="flex justify-center items-center w-2 h-2 rounded-full bg-[#FFB800]">
                      <div className="coin__front rounded-full w-1 h-1 bg-[#FF9900]"></div>
                    </div>
                    {item.earn}
                  </div>
                </div>
                <div className="col-span-3">
                  <button className="flex justify-center items-center gap-1 w-full text-[8px] text-white font-bold rounded-md bg-[#D9D9D950] py-1">
                    <img src={Branch} alt="branch"/>branch
                  </button>
                </div>
              </div>  
            ))
          }
        </div>
      </div>
      <button className="w-full text-xs font-normal text-white bg-[#0094FF] rounded-md my-4 px-4 py-3">invite a fren</button>
    </Wrapper>
  );
};

export default Frens;
