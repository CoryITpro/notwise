import { Link } from "react-router-dom";
import styled from "styled-components";
import Vector from "../../assets/Vector.svg";
import Right from "../../assets/icon-right.svg";
import TonIcon from "../../assets/icon-ton.svg";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 32px 16px;
  width: 100%;
  height: 100%;
`;

export const Shop = () => {

  return (
    <Wrapper className="bg-gradient-to-t from-[#5984AB] to-[#242424]">
      <div className="w-full">
        <div className="flex gap-2 mb-1">
          <button className="w-4 h-full mt-2"><img src={Vector} alt="Vector" /></button>
          <div>
            <h6 className="text-2xl font-bold text-white">Shop</h6>
            <p className="text-xs font-normal text-white">Here you can buy food and items</p>
          </div>
        </div>
        <div className="flex flex-col justify-center items-center mt-6 mb-8 gap-2 px-4">
          <div className="w-full">
            <div className="flex justify-between items-center mb-1">
              <p className="text-sm font-bold text-white">Food</p>
              <button className=""><img src={Right} alt="Right" /></button>
            </div>
            <div className="overflow-x-auto overflow-y-hidden">
              <div className="w-max flex gap-3">
              {
                [10, 20, 30, 11, 23, 42, 89].map((item, index) => (
                  <div key={index} className="flex flex-col justify-center items-center rounded-full">
                    <div className="rounded-2xl bg-[#D9D9D9] w-12 h-12 mb-1"></div>
                    <div className="flex justify-center items-center gap-1">
                      <div className="flex justify-center items-center w-[10px] h-[10px] rounded-full bg-[#FFB800]">
                        <div className="coin__front rounded-full w-[6px] h-[6px] bg-[#FF9900]"></div>
                      </div>
                      <p className="text-xs leading-3 font-bold text-white">{item}</p>
                    </div>
                  </div>
                ))
              }
              </div>
            </div>
          </div>
          <div className="w-full">
            <div className="flex justify-between items-center mb-1">
              <p className="text-sm font-bold text-white">Decorations</p>
              <button className=""><img src={Right} alt="Right" /></button>
            </div>
            <div className="overflow-x-auto overflow-y-hidden">
              <div className="w-max flex gap-3">
              {
                [100, 252, 374].map((item, index) => (
                  <div key={index} className="flex flex-col justify-center items-center rounded-full">
                    <div className="rounded-2xl bg-[#D9D9D9] w-12 h-12 mb-1"></div>
                    <div className="flex justify-center items-center gap-1">
                      <div className="flex justify-center items-center w-[10px] h-[10px] rounded-full bg-[#FFB800]">
                        <div className="coin__front rounded-full w-[6px] h-[6px] bg-[#FF9900]"></div>
                      </div>
                      <p className="text-xs leading-3 font-bold text-white">{item}</p>
                    </div>
                  </div>
                ))
              }
              </div>
            </div>
          </div>
          <div className="w-full">
            <div className="flex justify-between items-center mb-1">
              <p className="text-sm font-bold text-white">Eggs</p>
              <button className=""><img src={Right} alt="Right" /></button>
            </div>
            <div className="overflow-x-auto overflow-y-hidden">
              <div className="w-max flex gap-3">
              {
                [10, 20, 30, 11, 23, 42, 89].map((item, index) => (
                  <div key={index} className="flex flex-col justify-center items-center rounded-full">
                    <div className="rounded-2xl bg-[#D9D9D9] w-12 h-12 mb-1"></div>
                    <div className="flex justify-center items-center gap-1">
                      <img className="w-2 h-2" src={TonIcon} alt="TonIcon" />
                      <p className="text-xs leading-3 font-bold text-white">{item}</p>
                    </div>
                  </div>
                ))
              }
              </div>
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export default Shop;
