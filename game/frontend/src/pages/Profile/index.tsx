import { Link } from "react-router-dom";
import styled from "styled-components";
import Vector from "../../assets/Vector.svg";
import Right from "../../assets/icon-right.svg";
import Edit from "../../assets/icon-edit.svg";
import Add from "../../assets/icon-add.svg";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 32px 16px;
  width: 100%;
  height: 100%;
`;

export const Profile = () => {

  return (
    <Wrapper className="bg-gradient-to-t from-[#5984AB] to-[#242424]">
      <div className="w-full">
        <div className="flex gap-2 mb-1">
          <button className="w-4 h-full mt-2"><img src={Vector} alt="Vector" /></button>
          <div className="w-full flex justify-between">
            <div>
              <div className="flex items-center gap-2">
                <h6 className="text-2xl font-bold text-white">McSeemio</h6>
                <img src={Edit} alt="Edit"/>
              </div>
              <p className="text-sm font-normal text-[#607B93]">Durove’s Squad</p>
            </div>
            <div className="w-7 h-7 rounded-full bg-[#D9D9D9]"></div>
          </div>
        </div>
        <div className="flex flex-col justify-center items-center mt-8 gap-12 px-4">
          <div className="w-full flex justify-between px-2">
            <div>
              <p className="text-sm font-bold text-white mb-1">123745 <span className="text-[#607B93]">EXP</span></p>
              <div className="flex gap-1">
                <div className="flex justify-center items-center w-[10px] h-[10px] rounded-full bg-[#FFB800]">
                  <div className="coin__front rounded-full w-[6px] h-[6px] bg-[#FF9900]"></div>
                </div>
                <p className="text-xs leading-3 font-bold text-white">242</p>
              </div>
            </div>
            <div>
              <p className="text-sm font-bold text-white mb-1"><span className="text-[#607B93]">LVL</span> 13</p>
            </div>
          </div>
          <div className="w-full flex flex-col gap-3">
            <div className="grid grid-cols-10 gap-2">
              <p className="col-span-1 text-sm font-bold text-white">12</p>
              <div className="col-span-9 flex items-center gap-2 text-sm font-bold text-white">
                Strength <button><img className="w-4 h-4" src={Add} alt="Add"/></button>
              </div>
            </div>
            <div className="grid grid-cols-10 gap-2">
              <p className="col-span-1 text-sm font-bold text-white">3</p>
              <div className="col-span-9 flex items-center gap-2 text-sm font-bold text-white">
                Agility <button><img className="w-4 h-4" src={Add} alt="Add"/></button>
              </div>
            </div>
            <div className="grid grid-cols-10 gap-2">
              <p className="col-span-1 text-sm font-bold text-white">22</p>
              <div className="col-span-9 flex items-center gap-2 text-sm font-bold text-white">
                Wisdom <button><img className="w-4 h-4" src={Add} alt="Add"/></button>
              </div>
            </div>
            <div className="grid grid-cols-10 gap-2">
              <p className="col-span-1 text-sm font-bold text-white">11</p>
              <div className="col-span-9 flex items-center gap-2 text-sm font-bold text-white">
                Endurance <button><img className="w-4 h-4" src={Add} alt="Add"/></button>
              </div>
            </div>
          </div>
          <div className="w-full">
            <div className="flex justify-between items-center mb-3">
              <p className="text-sm font-bold text-white">Inventory</p>
              <button className=""><img src={Right} alt="Right" /></button>
            </div>
            <div className="overflow-x-auto overflow-y-hidden">
              <div className="w-max flex gap-1">
              {
                [10, 20, 30, 11].map((item, index) => (
                  <div key={index} className="flex flex-col justify-center items-center rounded-full">
                    <Link to={`/lootbox/${index}`} className="rounded-2xl bg-[#D9D9D9] w-12 h-12 mb-1"></Link>
                  </div>
                ))
              }
              </div>
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export default Profile;
