import { useContext } from "react";
import styled from "styled-components";
import { GameContext } from "../../App";
import Progressbar from "../../components/Progressbar";
import Slider from "../../components/Slider";
import OverflowerText from "../../components/OverflowerText";
const Wrapper = styled.div`
  display: flex;
  position: relative;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  padding: 48px 32px;
  width: 100%;
  height: 100%;
`;

const Stage1 = () => {
  const { gameUserInfo } = useContext(GameContext);

  const calcTemperature = (heats: number) => {
    if (heats > 50) return 10; // 10px
    else if (heats < 0) return 190; // 200px - 10px
    else return (190 - (180 / 50) * heats);
  };

  return (
    <Wrapper>
      <Progressbar
        barcolor="bg-gradient-to-r from-[#FFF173] to-[#99FFAA]"
        ht="min-h-2"
        percent="w-2/3"
        value={null}
        maxvalue={null}
      />
      <div className="relative flex justify-center text-center w-full h-[120px] my-16">
        {gameUserInfo &&
          gameUserInfo.experiences.length > 0 &&
          gameUserInfo.experiences.map((exp: number, idx: number) => (
            <OverflowerText
              key={idx}
              className="absolute bottom-0 text-lg text-white font-bold translate-x-1/2"
            >
              {exp > 0 ? `+${exp} EXP`: `${exp} EXP`}
            </OverflowerText>
          ))}
      </div>
      <div className="relative w-full h-auto">
        <Slider
          containerClassName="relative w-4 h-[200px]" // absolute left-10 top-1/3 translate-y-1/2
          progressClassName={`w-5 h-[200px] absolute top-0 left-1/2 -translate-x-1/2 bg-gradient-to-t rounded-lg from-[#34C2FF] via-[#7DFF5C] to-[#FF6847]`}
          styles={{clipPath: `circle(10px at center ${calcTemperature(gameUserInfo.heats)}px)`}}
        />
      </div>
    </Wrapper>
  );
};

export default Stage1;
