import styled from "styled-components";
import GameScene from "../../components/GameScene";

const Wrapper = styled.div`
  position: absolute;
  background: transparent;
  // z-index: -1;
`;

export const Editor = (gameActionManager: any) => {
  const action = gameActionManager;
  return (
    <Wrapper className="w-full h-full">
      <GameScene gameActionManager={action.gameActionManager} />
    </Wrapper>
  );
};

export default Editor;
