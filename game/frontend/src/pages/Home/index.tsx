import styled from "styled-components";
import { useNavigate } from "react-router-dom";
import { useTonAddress, useTonConnectUI } from "@tonconnect/ui-react";
import { useEffect } from "react";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 32px 16px;
  height: 100%;
`;

export const Home = () => {
  const navigate = useNavigate();
  // const { state, open, close } = useTonConnectModal();
  const userFriendlyAddress = useTonAddress();
  const [tonConnectUI] = useTonConnectUI();

  const handleWallet = async () => {
    if (!tonConnectUI.connected) {
      await tonConnectUI.openModal();
    } else {
      await tonConnectUI.disconnect();
    }
  };

  useEffect(() => {
    if (tonConnectUI.connected) navigate("/stage1");
  }, [tonConnectUI.connected, navigate]);

  return (
    <Wrapper className="bg-gradient-to-t from-[#5984AB] to-[#242424]">
      <div className="text-center my-20">
        <h1 className="text-3xl text-white font-bold mb-4">Hey 👋</h1>
        <p className="text-sm text-white font-normal">
          Early access is only available for NOT Wise owl holders
        </p>
      </div>
      <button
        className="text-sm text-white font-normal w-full rounded-md bg-[#0094FF] mb-8 px-8 py-2 pointer-events-auto"
        onClick={handleWallet}
      >
        {tonConnectUI.connected ? (
          <>
            Disconnect wallet: {userFriendlyAddress.slice(0, 6)}...
            {userFriendlyAddress.slice(-6)}
          </>
        ) : (
          "Connect wallet"
        )}
      </button>
    </Wrapper>
  );
};

export default Home;
