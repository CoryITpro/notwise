import * as THREE from "three";

export const RENDERER_PROPS = {
  antialias: true,
  outputEncoding: THREE.sRGBEncoding,
  toneMapping: THREE.ACESFilmicToneMapping,
  shadowMapEnable: true,
};

export const CAMERA_PROPS = {
  fov: 45,
  near: 0.1,
  far: 1000,
  position: {
    x: 0,
    y: 0,
    z: 55,
  },
};

export const SCENE_PROPS = {
  fog: {
    enable: true,
    color: 0xffffff,
    near: 90,
    far: 200,
  },
};
