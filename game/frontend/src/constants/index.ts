export const MODEL_URLS = {
  owl: "/assets/models/owl.glb",
  egg: "/assets/models/egg.glb",
  tree: "/assets/models/tree.glb",
};

export const ANIMATION_NAMES = [
  "idle",
  "falls_asleep_v1",
  "wakes_up_v1",
  "falls_asleep_v2",
  "wakes_up_v2",
  "sleep",
  "agressive_v1",
  "attack_v1",
  "agressive_v2",
  "attack_v2",
  "satisfied_v1",
  "satisfied_v2",
  "look_with_interest_v1",
  "look_with_interest_v2",
  "owl_is_ill_v1",
  "owl_is_ill_v2",
];

export const ANIMATION_TIMELINE: any = {
  idle: {
    startFrame: 10,
    endFrame: 1030,
  },
  falls_asleep_v1: {
    startFrame: 1040,
    endFrame: 1275,
  },
  wakes_up_v1: {
    startFrame: 1285,
    endFrame: 1320,
  },

  falls_asleep_v2: {
    startFrame: 1330,
    endFrame: 1445,
  },

  wakes_up_v2: {
    startFrame: 1455,
    endFrame: 1635,
  },
  sleep: {
    startFrame: 1645,
    endFrame: 1925,
  },
  agressive_v1: {
    startFrame: 1935,
    endFrame: 2040,
  },
  attack_v1: {
    startFrame: 2050,
    endFrame: 2075,
  },
  agressive_v2: {
    startFrame: 2085,
    endFrame: 2315,
  },
  attack_v2: {
    startFrame: 2325,
    endFrame: 2370,
  },
  satisfied_v1: {
    startFrame: 2380,
    endFrame: 2470,
  },
  satisfied_v2: {
    startFrame: 2480,
    endFrame: 2565,
  },
  look_with_interest_v1: {
    startFrame: 2575,
    endFrame: 2925,
  },
  look_with_interest_v2: {
    startFrame: 2935,
    endFrame: 3285,
  },
  owl_is_ill_v1: {
    startFrame: 3295,
    endFrame: 3615,
  },
  owl_is_ill_v2: {
    startFrame: 3625,
    endFrame: 3945,
  },
};

export const SQUAD_ARRAY = [
  { exp: 162366, rate: 50 },
  { exp: 162322, rate: 25 },
  { exp: 162361, rate: 12.5 },
  { exp: 162336, rate: 6.25 },
  { exp: 162376, rate: 1.01 },
];

export const FRENS_ARRAY = [
  { username: "mcseemio", earn: 330 },
  { username: "user1", earn: 130 },
  { username: "user2", earn: 30 },
];
