import { createContext, useCallback, useEffect, useRef, useState } from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import styled from "styled-components";
import {THEME, TonConnectUIProvider} from "@tonconnect/ui-react";

import Home from "./pages/Home";
import Stage1 from "./pages/Stage/Stage1";
import Stage2 from "./pages/Stage/Stage2";
import Stage3 from "./pages/Stage/Stage3";
import Editor from "./pages/Editor";
import Squad from "./pages/Squad";
import LeaderBoard from "./pages/LeaderBoard";
import Tree from "./pages/Tree";
import Fight from "./pages/Fight";
import Shop from "./pages/Shop";
import Item from "./pages/Item";
import Frens from "./pages/Frens";
import Profile from "./pages/Profile";
import Lootbox from "./pages/Lootbox";

const Wrapper = styled.div`
  position: absolute;
  background: transparent;
  width: 100%;
  height: 100%;
  z-index: 1;
  pointer-events: none;
`;

export interface UserInfo {
  experiences: number[];
  heats: number;
  frens: any[]|null;
  items: any[]|null;
}

export const GameContext = createContext<any>(null);

function App() {
  const [gameRef, setGameRef] = useState<any>(null);
  const [gameUserInfo, setGameUserInfo] = useState<UserInfo>({
    experiences: [],
    heats: 0,
    frens: null,
    items: null
  });
  let heat = useRef(gameUserInfo.heats);

  const gameActionManager = (userAction: string) =>{
    switch(userAction) {
      case 'swipeowl':
        swipeOwl();
        break;
      default:
        break;
    }
  }

  const swipeOwl = useCallback(() => {
    let exp = 0;

    if (heat.current > 50) { // if temperature is over 50
      heat.current = 50;
    } else if (heat.current < 0) { // if temperature is under 0
      heat.current = 0;
    } else {
      heat.current += 1;
      if (heat.current > 34) exp = -(Math.floor(Math.random() * 50) + 1);
      else exp = Math.floor(Math.random() * 50) + 1;
    }

    setGameUserInfo((t) => {
      return {
        experiences: t ? [...t.experiences, exp] : [exp],
        heats: heat.current,
        frens: t.frens ? [...t.frens] : null,
        items: t.items ? [...t.items] : null
      };
    });
  }, [gameUserInfo])

  useEffect(() => {
    const interval = setInterval(() => {
      setGameUserInfo((t) => {
        return {
          experiences: t.experiences,
          heats: t.heats > 0 ? t.heats > 50 ? 50 : heat.current - 1 : 0,
          frens: t.frens ? [...t.frens] : null,
          items: t.items ? [...t.items] : null
        };
      });
      console.log("---------heat", heat.current)
      heat.current > 0 ? heat.current -= 1 : heat.current = 0;
    }, 5*1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <TonConnectUIProvider
      manifestUrl="https://ton-connect.github.io/demo-dapp-with-react-ui/tonconnect-manifest.json"
      uiPreferences={{ theme: THEME.DARK }}
      walletsListConfiguration={{
        includeWallets: [
          {
            appName: "safepalwallet",
            name: "SafePal",
            imageUrl: "https://s.pvcliping.com/web/public_image/SafePal_x288.png",
            aboutUrl: "https://www.safepal.com/download",
            jsBridgeKey: "safepalwallet",
            platforms: ["ios", "android", "chrome", "firefox"]
          },
          {
            appName: "tonwallet",
            name: "TON Wallet",
            imageUrl: "https://wallet.ton.org/assets/ui/qr-logo.png",
            aboutUrl: "https://chrome.google.com/webstore/detail/ton-wallet/nphplpgoakhhjchkkhmiggakijnkhfnd",
            universalLink: "https://wallet.ton.org/ton-connect",
            jsBridgeKey: "tonwallet",
            bridgeUrl: "https://bridge.tonapi.io/bridge",
            platforms: ["chrome", "android"]
          }
        ]
      }}
    >
      <GameContext.Provider value={{gameRef, setGameRef, gameUserInfo, setGameUserInfo }}>
        <div className="App w-screen h-screen">
          <Editor gameActionManager={gameActionManager} />
          <Wrapper>
            <Routes>
              <Route path="/stage1" element={<Stage1 />} />
              <Route path="/stage2" element={<Stage2 />} />
              <Route path="/stage3" element={<Stage3 />} />
              <Route path="/squad" element={<Squad />} />
              <Route path="/squad/:id" element={<LeaderBoard />} />
              <Route path="/fight" element={<Fight />} />
              <Route path="/fight-tree" element={<Tree />} />
              <Route path="/shop" element={<Shop />} />
              <Route path="/item" element={<Item />} />
              <Route path="/frens" element={<Frens />} />
              <Route path="/profile" element={<Profile />} />
              <Route path="/lootbox/:id" element={<Lootbox />} />
              <Route path="/" element={<Home />} />
              <Route path="*" element={<Navigate to="/" />} />
            </Routes>
          </Wrapper>
        </div>
      </GameContext.Provider>
    </TonConnectUIProvider>
  );
}

export default App;
