import { ThemeConsumer } from "styled-components";
import AssetsManager from "./AssetsManager";
import { SceneRenderer } from "./rendering/SceneRenderer";
import * as THREE from "three";
import AnimationManager from "./AnimationManager";
import { ANIMATION_NAMES } from "../../constants";
import UserActionManager from "./UserActionManager";

export interface GameOptions {
  canvas: HTMLDivElement;
  assetsManager: AssetsManager;
  animationManager: AnimationManager;
  gameActionManager: any;
}

export class Game {
  _canvasDiv: HTMLDivElement;
  _assetsManager: AssetsManager;
  _animationManager: AnimationManager;
  _sceneRenderer: SceneRenderer;
  _userActionManager: UserActionManager;

  constructor(options: GameOptions) {
    this._canvasDiv = options.canvas;

    this._assetsManager = options.assetsManager;

    this._animationManager = options.animationManager;

    this._sceneRenderer = new SceneRenderer();

    this._userActionManager = new UserActionManager(
      options,
      this._sceneRenderer._camera,
      this._sceneRenderer._scene
    );

    this.initialize();
  }

  initialize() {
    this._canvasDiv.appendChild(this._sceneRenderer._renderer.domElement);

    this._sceneRenderer._scene.add(this._assetsManager._models.egg);

    // this._sceneRenderer._scene.add(this._assetsManager._models.owl);

    this._sceneRenderer._scene.add(this._assetsManager._models.tree);

    this.animate();
  }

  animate() {
    requestAnimationFrame(this.animate.bind(this));
    this._sceneRenderer.render();
  }
}
