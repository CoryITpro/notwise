import { convertUV } from "../../utils/three";
import AnimationManager from "./AnimationManager";
import AssetsManager from "./AssetsManager";
import { GameOptions } from "./game";
import * as THREE from "three";
// import { SceneRenderer } from "./rendering/SceneRenderer";
// import { ANIMATION_NAMES } from "../../constants";

type TouchStatus = {
  position: THREE.Vector2;
  timeStamp: number;
};

export class UserActionManager {
  _canvasDiv: HTMLDivElement;
  _assetsManager: AssetsManager;
  _animationManager: AnimationManager;
  _camera: THREE.Camera;
  _raycaster: THREE.Raycaster;
  _scene: THREE.Scene;
  _gameActionManager: any;
  _touchStatus: TouchStatus | null;

  constructor(options: GameOptions, camera: THREE.Camera, scene: THREE.Scene) {
    this._canvasDiv = options.canvas;

    this._assetsManager = options.assetsManager;

    this._animationManager = options.animationManager;

    this._camera = camera;

    this._raycaster = new THREE.Raycaster();

    this._scene = scene;

    this._gameActionManager = options?.gameActionManager;

    this._touchStatus = null;

    this.initEventHandler();
  }

  initEventHandler() {
    this._canvasDiv.addEventListener(
      "touchmove",
      this.touchMoveEventHandler.bind(this)
    );

    this._canvasDiv.addEventListener("touchstart", (ev) =>
      this.setTouchStatus(
        ev.changedTouches[0].clientX,
        ev.changedTouches[0].clientY
      )
    );

    this._canvasDiv.addEventListener(
      "touchend",
      () => (this._touchStatus = null)
    );

    // this._canvasDiv.addEventListener(
    //   "swipeleft",
    //   this.pointerSwipeEventHandler.bind(this)
    // );
    // this._canvasDiv.addEventListener(
    //   "swiperight",
    //   this.pointerSwipeEventHandler.bind(this)
    // );
    // this._canvasDiv.addEventListener(
    //   "swipedown",
    //   this.pointerSwipeEventHandler.bind(this)
    // );
    // this._canvasDiv.addEventListener(
    //   "swipeup",
    //   this.pointerSwipeEventHandler.bind(this)
    // );
    // this._canvasDiv.addEventListener("swipemove", (ev) => {
    //   console.log("move", ev);
    // });
    // this._canvasDiv.addEventListener("tap", () => {
    //   console.log("tap");
    // });
  }

  setTouchStatus(x: number, y: number) {
    this._touchStatus = {
      position: new THREE.Vector2(x, y),
      timeStamp: Date.now(),
    };
  }

  isOnEgg(x: number, y: number) {
    this._raycaster.setFromCamera(convertUV(x, y), this._camera);

    const intersects = this._raycaster.intersectObjects(
      this._assetsManager._models.egg.children
    );

    return intersects.length > 0 ? true : false;
  }

  // pointerSwipeEventHandler(ev: any) {
  //   console.log(ev);

  //   // console.log(ev.detail.coords.startX, ev.detail.coords.startY);
  //   // console.log(ev.detail.coords.endX, ev.detail.coords.endY);

  //   if (
  //     this.isOnEgg(
  //       ev.detail.coords.startX - 966,
  //       ev.detail.coords.startY - 262
  //     ) &&
  //     this.isOnEgg(ev.detail.coords.endX - 966, ev.detail.coords.endY - 262)
  //   ) {
  //     console.log("swipe");
  //     this._gameActionManager("swipeowl");
  //   }
  // }

  touchMoveEventHandler(ev: any) {
    if (
      this.isOnEgg(ev.changedTouches[0].clientX, ev.changedTouches[0].clientY)
    ) {
      if (this._touchStatus === null) return;

      const oldTouchStatus: TouchStatus = this._touchStatus;

      this.setTouchStatus(
        ev.changedTouches[0].clientX,
        ev.changedTouches[0].clientY
      );

      const moveOffset = this._touchStatus.position.distanceTo(
        oldTouchStatus.position
      );
      const timeOffset = this._touchStatus.timeStamp - oldTouchStatus.timeStamp;

      console.log(moveOffset, timeOffset);

      if (moveOffset / timeOffset > 1 / 2) {
        console.log("swipe");
        this._gameActionManager("swipeowl");
      }
    }
  }
}
export default UserActionManager;
