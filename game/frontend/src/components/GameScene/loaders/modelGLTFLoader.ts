import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

export const LoadModelGLTF = (src: any, shadow = true, transparent = true) => {
  return new Promise((resolve) => {
    const loader = new GLTFLoader();

    loader.load(src, function (object) {
      object.scene.children[0].traverse((obj: any) => {
        if (shadow && (obj.isMesh || obj.isSkinnedMesh)) {
          obj.castShadow = true;
          obj.receiveShadow = true;
          obj.material.transparent = transparent;
          if (obj.material.map) obj.material.map.anisotropy = 16;
        }
      });
      resolve(object);
    });
  });
};
