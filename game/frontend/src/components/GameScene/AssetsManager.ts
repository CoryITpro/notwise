// import * as THREE from "three";
import {
  ANIMATION_NAMES,
  ANIMATION_TIMELINE,
  MODEL_URLS,
} from "../../constants";
import { LoadModelGLTF } from "./loaders/modelGLTFLoader";
import * as THREE from "three";

export class AssetsManager {
  _models: any;
  _animations: any;

  constructor() {
    this._models = {};
    this._animations = {};
  }

  loadModels() {
    return new Promise(async (resolve, reject) => {
      const owlObject = (await LoadModelGLTF(MODEL_URLS["owl"])) as any;
      this._models.owl = owlObject.scene.children[0];

      const treeObject = (await LoadModelGLTF(MODEL_URLS["tree"])) as any;
      this._models.tree = treeObject.scene.children[0];

      const eggObject = (await LoadModelGLTF(MODEL_URLS["egg"])) as any;
      this._models.egg = eggObject.scene.children[0];

      this.initModels();
      this.initAnimationClip(owlObject.animations[0]);

      resolve(true);
    });
  }
  initModels() {
    // this._models.tree.children[3].visible = false;
    // this._models.tree.position.set(-2, -3, -2);
    // this._models.egg.position.set(-2, -4, -2);
    // this._models.egg.rotation.set(0, 0, -0.3);
    // console.log(this._models.tree.position);
  }
  initAnimationClip(animation: THREE.AnimationClip) {
    ANIMATION_NAMES.forEach((name: string) => {
      this._animations[name] = THREE.AnimationUtils.subclip(
        animation,
        name,
        ANIMATION_TIMELINE[name].startFrame,
        ANIMATION_TIMELINE[name].endFrame,
        30
      );
    });
  }

  getModel(type: number) {
    return this._models[type];
  }
}

export default AssetsManager;
