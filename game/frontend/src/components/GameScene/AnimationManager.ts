import * as THREE from "three";
import AssetsManager from "./AssetsManager";
import { ANIMATION_NAMES, ANIMATION_TIMELINE } from "../../constants";

export class AnimationManager {
  _actions: any;
  _model: any;
  _animations: any;
  _clock: any;
  _mixer: any;
  _activeAction: THREE.AnimationAction | null;
  _lastAction: THREE.AnimationAction | null;

  constructor(assetsManager: AssetsManager) {
    this._actions = {};
    this._model = assetsManager._models.owl;
    this._animations = assetsManager._animations;
    this._clock = new THREE.Clock();
    this._activeAction = null;
    this._lastAction = null;
    this._mixer = new THREE.AnimationMixer(assetsManager._models.owl);

    this.initActions();
    this.animation();
  }

  initActions() {
    ANIMATION_NAMES.forEach((name: string) => {
      this._actions[name] = this._mixer.clipAction(this._animations[name]);
    });
    this.setAction(this._actions["idle"]);
  }

  getAction(type: string) {
    if (!this._actions[type]) {
      console.error("no such animation of ", type);
      return;
    }
    return this._actions[type];
  }

  playAction(type: string) {
    if (!this.getAction(type)) return;
    this.setAction(this.getAction(type));
  }

  setAction(toAction: THREE.AnimationAction) {
    if (toAction != this._activeAction) {
      this._lastAction = this._activeAction;
      this._activeAction = toAction;
      this._lastAction?.fadeOut(1);
      this._activeAction.reset();
      this._activeAction.fadeIn(1);
      this._activeAction.play();
    }
  }

  animation() {
    requestAnimationFrame(this.animation.bind(this));
    this._mixer.update(this._clock.getDelta());
  }
}

export default AnimationManager;
