import { useCallback, useContext, useEffect, useRef } from "react";
import styled from "styled-components";
import { Game } from "./game";
import { AssetsManager } from "./AssetsManager";
import AnimationManager from "./AnimationManager";
import { GameContext } from "../../App";
import TouchSweep from "touchsweep";

const Wrapper = styled.div`
  position: relative;
  width: 100vw;
  height: 100vh;
`;

export const GameScene = (gameActionManager: any) => {
  const { gameRef, setGameRef } = useContext(GameContext);
  const firstRef = useRef(false);

  const canvasDivRef = useRef(null);
  // const gameRef = useRef(null) as any;
  const assetsManagerRef = useRef(null) as any;
  const animationManagerRef = useRef(null) as any;

  const createGame = useCallback(async () => {
    assetsManagerRef.current = new AssetsManager();

    await assetsManagerRef.current.loadModels();

    animationManagerRef.current = new AnimationManager(
      assetsManagerRef.current
    );

    startGame();
  }, []);

  const startGame = () => {
    if (gameRef?.current) return;

    const data = {
      value: 10,
    };

    const touchThreshold = 20;

    const touchSweepInstance = new TouchSweep(
      canvasDivRef.current!,
      data,
      touchThreshold
    );

    let newGameRef = new Game({
      canvas: canvasDivRef.current!,
      assetsManager: assetsManagerRef.current,
      animationManager: animationManagerRef.current,
      gameActionManager: gameActionManager.gameActionManager,
    });
    setGameRef({ gameRef: { current: newGameRef } });
  };

  useEffect(() => {
    if (firstRef.current) return;
    firstRef.current = true;

    createGame();
  }, []);

  return (
    <Wrapper>
      <div ref={canvasDivRef}></div>
    </Wrapper>
  );
};

export default GameScene;
