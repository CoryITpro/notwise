interface ProgressbarProps {
  barcolor: null | string;
  ht: null | string;
  percent: null | string;
  value: null | number | string;
  maxvalue: null | number | string;
}

const Progressbar = (props: ProgressbarProps) => {
  const { barcolor, ht, percent, value=null, maxvalue=null } = props;
  return (
    <div className="w-full">
      <div
        className={`flex justify-center relative w-full ${ht} bg-[#D9D9D950] rounded-full overflow-hidden`}
      >
        <div
          id="progressBar"
          className={`absolute top-0 left-0 ${percent} h-full ${barcolor} rounded-full transition-width duration-500`}
        ></div>
        {value && maxvalue ? (
          <p className="text-[8px] text-white text-center z-10">
            {value} / {maxvalue}
          </p>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
};
export default Progressbar;
