import Sliderbar from "../../assets/icon-sliderbar.svg";

interface SliderProps {
  containerClassName: string;
  progressClassName: string;
  styles: React.CSSProperties;
}

const Slider = (props: SliderProps) => (
  <div className={`slider-container pointer-events-auto ${props.containerClassName}`}>
    <img src={Sliderbar} className="w-full h-full" alt="sliderbar" />
    <div className={`pointer-events-auto ${props.progressClassName}`} style={{ ...props.styles }}></div>
  </div>
);

export default Slider;
