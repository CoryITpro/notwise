interface OverflowerTextProps {
  children: React.ReactNode;
  className?: string;
}

const OverflowerText = (props: OverflowerTextProps) => (
  <div className={`overflow-text${props.className? ` ${props.className}` : ''}`}>{props.children}
  </div>
);

export default OverflowerText;
