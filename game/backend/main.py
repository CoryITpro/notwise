from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from src.config.settings import DATABASE_URI, APPS_MODELS
from src.auth.config import auth_backend, fastapi_users
from tortoise.contrib.fastapi import register_tortoise

app = FastAPI(
    title="Notwise"
)

app.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/auth",
    tags=["auth"],
)



origins = ['*']
    
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "OPTIONS", "DELETE", "PATCH", "PUT"],
    allow_headers=["Content-Type", "Set-Cookie", "Access-Control-Allow-Headers", "Access-Control-Allow-Origin",
                   "Authorization"],
)


register_tortoise(
    app,
    db_url=DATABASE_URI,
    modules={"models": APPS_MODELS},
    generate_schemas=True,
    add_exception_handlers=True,
)