import os

from dotenv import load_dotenv

load_dotenv()

DATABASE_URI = "sqlite://db.sqlite3"

SECRET_AUTH = os.environ.get("SECRET_AUTH")

APPS_MODELS = [
    "src.auth.models",
    "aerich.models",
]