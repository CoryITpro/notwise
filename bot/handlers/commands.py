from aiogram import Dispatcher, types
from db.models import User

async def cmd_start(message: types.Message):
    print(await User.all())

    await User.create(tg_id=message.chat.id, username = message.chat.username)
    markup = types.InlineKeyboardMarkup(
        inline_keyboard=[
            [
                types.InlineKeyboardButton(
                    text="Run 3D",
                    web_app=types.WebAppInfo(url=f'https://notwise.org/'),
                )
            ]
        ]
    )
    await message.answer("Hi, Browl! Something great is going to appear here", reply_markup=markup)


def register_commands(dp: Dispatcher):
    dp.register_message_handler(cmd_start, commands="start", state="*")