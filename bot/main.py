import os
import asyncio
import logging
from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from tortoise import Tortoise, run_async
from handlers.commands import register_commands
from dotenv import load_dotenv

load_dotenv()

async def init():
    await Tortoise.init(
        db_url='sqlite://db.sqlite3',
        modules={'models': ['db.models']},
    )
    await Tortoise.generate_schemas()


async def main():

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
    )

    bot = Bot(os.getenv('BOT'), parse_mode="HTML")
    dp = Dispatcher(bot, storage=MemoryStorage())

    register_commands(dp)

    await dp.start_polling()

try:
    run_async(init())
    asyncio.run(main())
except (KeyboardInterrupt, SystemExit):
    logging.error("Bot stopped!")