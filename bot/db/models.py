from tortoise import fields
from tortoise.models import Model

class User(Model):
    id = fields.IntField(pk=True)
    tg_id = fields.IntField()
    username = fields.CharField(max_length=255)